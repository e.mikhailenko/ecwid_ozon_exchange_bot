def get_category_mapping_dict():
    # словарь сопоставленных id категорий товаров Ecwid-Ozon
    # key - ecwid_category_id, value - ozon_category_id
    category_mapping_dict = {
        '71212875': 17027904,  # Кросс-боди - Сумка
        '71212877': 41777462,  # Спортивные товары Другое - Спортивная одежда
        '71448230': 15621031,  # Одежда - Одежда
        '71448231': 41777454,  # Брюки - Брюки, джинсы, бриджи
        '71448232': 38148486,  # Леггинсы - Леггинсы, лосины женские
        '71448234': 17032630,  # Макси платья - Платье, сарафан женские
        '71448237': 41777459,  # Блузы - Блузки и рубашки
        '71448243': 41777453,  # Кардиганы - Свитеры, кардиганы, толстовки
        '71448245': 41777456,  # Джинсовые шорты - Шорты
        '71448248': 17027899,  # Ювелирные украшения - Бижутерия
        '71448249': 17027899,  # Кольца - Бижутерия
        '71448251': 22825038,  # Ветровки и куртки - Куртка женская
        '71448253': 38148445,  # Компрессионные носки - Носки женские
        '71448266': 41777461,  # Нижнее белье - Белье и купальники
        '71448267': 41777461,  # Бра - Белье и купальники
        '71448269': 41777457,  # Ромперы - Комбинезоны
        '71448273': 17038452,  # Платки - Женский платок на шею
        '71448276': 22825038,  # Ветровки и куртки - Куртка женская
        '71448279': 17027906,  # Décor - Аксессуары для интерьера
        '71448282': 41777465,  # Зимние шапки - Шапки, шарфы, перчатки
        '71448285': 17035165,  # Ободки - Ободок для волос
        '71448288': 41777461,  # Бикини - Белье и купальники
        '71448233': 41777452,  # Платья - Платья и юбки
        '71448239': 41777454,  # Джинсы - Брюки, джинсы, бриджи
        '71448240': 41777453,  # Кофты и толстовки - Свитеры, кардиганы, толстовки
        '71448247': 17027493,  # Личные аксессуары - Галантерея и украшения
        '71448252': 38148445,  # Носки, чулки и колготки - Носки женские
        '71448254': 41777452,  # Платья для особых случаев - Платья и юбки
        '71448257': 41777454,  # Шаровары - Брюки, джинсы, бриджи
        '71448260': 17032631,  # Пуловеры - Пуловер женский
        '71448270': 38148549,  # Гольфы - Гольфы женские
        '71448277': 17027898,  # Часы - Часы
        '71448289': 40893835,  # Тренчи - Верхняя одежда
        '71448292': 38146916,  # Слитные купальники - Купальник слитный
        '71212876': 17032621,  # Пончо - Пончо женское
        '71448235': 17032640,  # Юбки - Юбка женская
        '71448241': 41777460,  # Футболки - Футболки и топы
        '71448256': 38148486,  # Штаны для йоги - Леггинсы, лосины женские
        '71448259': 41777462,  # Спортивные товары - Спортивная одежда
        '71448271': 38148445,  # Носки - Носки женские
        '71448272': 41777465,  # Шарфы и шали - Шапки, шарфы, перчатки
        '71448283': 17034340,  # Серьги - Бижу комплект серьги женские
        '71448290': 22825038,  # Флисовые куртки - Куртка женская
        '71448293': 41777452,  # Коктейльные платья - Платья и юбки
        '71212873': 41777452,  # Коктейльные платье - Платья и юбки
        '71448236': 41777460,  # Футболки и топы - Футболки и топы
        '71448246': 17032635,  # Топы - Топ женский
        '71448265': 41777461,  # Пижамы и нижнее белье - Белье и купальники
        '71448268': 22825105,  # Капри - Бриджи, капри женские
        '71448280': 29183107,  # Аксессуары для волос - Аксессуары для волос
        '71448284': 17027899,  # Браслеты - Бижутерия
        '71448291': 17032619,  # Пальто - Пальто женское
        '71212869': 17032619,  # Полупальто - Пальто женское
        '71212878': 22825732,  # Спортивные штаны - Спортивные брюки женские
        '71448238': 41777457,  # Комбинезоны - Комбинезоны
        '71448255': 17032635,  # Топы с запахом - Топ женский
        '71448278': 17027906,  # Дом - Аксессуары для интерьера
        '71448281': 17038447,  # Шляпы - Женская шляпа
        '71448286': 17027899,  # Украшения на шею - Бижутерия
        '71212870': 17032622,  # Пуховики - Пуховик женский
        '71212874': 17027904,  # Сумки - Сумка
        '71448242': 17032633,  # Свитера - Свитер женский
        '71448261': 17032635,  # топы - Топ женский
        '71212871': 17032620,  # Плащи - Плащ женский
        '71448244': 41777456,  # Шорты - Шорты
        '71448263': 17032638,  # Туники - Туника женская
        '71448250': 40893835,  # Верхняя одежда - Верхняя одежда
        '71448264': 17033665,  # Водолазки - Водолазка женская
        '71448258': 40893835,  # Другое - Верхняя одежда
        '71448274': 17032637,  # Футболки поло - Поло женское
        '71212872': 17032632,  # Рубашки - Рубашка женская
        '71448262': 41777457,  # Цельный одежды - Комбинезоны
        '71448275': 17033668,  # Жакеты - Жакет женский
        '71448287': 38146915,  # Купальники - Купальный костюм
    }
    return category_mapping_dict


def get_name_category_dict(ozon_category_id):
    name_category_dict = {
        '15621031': 'Одежда',
        '17032630': 'Платье, сарафан женские',
        '41777459': 'Блузки и рубашки',
        '17038452': 'Женский платок на шею',
        '17035165': 'Ободок для волос',
        '41777453': 'Свитеры, кардиганы, толстовки',
        '17027493': 'Галантерея и украшения',
        '41777454': 'Брюки, джинсы, бриджи',
        '17032631': 'Пуловер женский',
        '38148549': 'Гольфы женские',
        '17027898': 'Часы',
        '38146916': 'Купальник слитный',
        '17032621': 'Пончо женское',
        '17032640': 'Юбка женская',
        '38148486': 'Леггинсы, лосины женские',
        '41777462': 'Спортивная одежда',
        '38148445': 'Носки женские',
        '41777465': 'Шапки, шарфы, перчатки',
        '17034340': 'Бижу комплект серьги женские',
        '22825038': 'Куртка женская',
        '41777452': 'Платья и юбки',
        '41777460': 'Футболки и топы',
        '41777461': 'Белье и купальники',
        '22825105': 'Бриджи, капри женские',
        '29183107': 'Аксессуары для волос',
        '17032619': 'Пальто женское',
        '22825732': 'Спортивные брюки женские',
        '17027906': 'Аксессуары для интерьера',
        '17038447': 'Женская шляпа',
        '17027899': 'Бижутерия',
        '17032622': 'Пуховик женский',
        '17027904': 'Сумка',
        '17032633': 'Свитер женский',
        '17032635': 'Топ женский',
        '17032620': 'Плащ женский',
        '41777456': 'Шорты',
        '17032638': 'Туника женская',
        '17033665': 'Водолазка женская',
        '40893835': 'Верхняя одежда',
        '17032637': 'Поло женское',
        '17032632': 'Рубашка женская',
        '41777457': 'Комбинезоны',
        '17033668': 'Жакет женский',
        '38146915': 'Купальный костюм'
    }
    return name_category_dict[str(ozon_category_id)]


def get_test_request_data_for_import_ozon():
    test_request_data = {
        'items': [
            {
                'attributes': [
                    {
                        'complex_id': 0,
                        'id': 0,
                        'values': [
                            {
                                'dictionary_value_id': 0,
                                'value': 'string'
                            }
                        ]
                    }
                ],
                'barcode': 'string',
                'category_id': 0,
                'complex_attributes': [
                    {
                        'attributes': [
                            {
                                'complex_id': 0,
                                'id': 0,
                                'values': [
                                    {
                                        'dictionary_value_id': 0,
                                        'value': 'string'
                                    }
                                ]
                            }
                        ]
                    }
                ],
                'depth': 0,
                'dimension_unit': 'string',
                'height': 0,
                'image_group_id': 'string',
                'images': [
                    'string'
                ],
                'images360': [
                    'string'
                ],
                'name': 'string',
                'offer_id': 'string',
                'old_price': 'string',
                'pdf_list': [
                    {
                        'index': 0,
                        'name': 'string',
                        'src_url': 'string'
                    }
                ],
                'premium_price': 'string',
                'price': 'string',
                'vat': 'string',
                'weight': 0,
                'weight_unit': 'string',
                'width': 0
            }
        ]
    }
    return test_request_data


# Одежда и все подкатегории
def get_category_clothes_id_list():
    category_clothes_id_list = [
        41777462, 15621031, 41777454, 38148486, 17032630, 41777459, 41777453, 41777456,
        22825038, 38148445, 41777461, 41777461,
        41777457, 17038452, 22825038, 41777465, 41777461, 41777452, 41777454, 41777453,
        38148445, 41777452, 41777454, 17032631, 38148549, 40893835, 38146916, 17032621,
        17032640, 41777460, 38148486, 41777462, 22825105,
        38148445, 41777465, 22825038, 41777452, 41777452, 41777460, 17032635, 41777461,
        17032619, 17032619, 22825732, 41777457, 17032635, 17038447, 17032622, 17032633,
        17032635, 17032620, 41777456, 17032638,
        40893835, 17033665, 40893835, 17032637, 17032632, 41777457, 17033668, 38146915
    ]
    return category_clothes_id_list


# Товары для интерьера
def get_category_interior_id_list():
    category_interior_id_list = [17027906]
    return category_interior_id_list


# Украшения - бижутерия, часы, ювелирные изделия
def get_category_decoration_id_list():
    category_decoration_id_list = [
        17027899, 17027898, 17034340,
        17027899, 17027899
    ]
    return category_decoration_id_list


# Аксессуары - сумки, аксессуары для волос, очки и т.д.
def get_category_accessories_id_list():
    category_accessories_id_list = [
        17035165, 17027493, 29183107, 17027904
    ]
    return category_accessories_id_list


def get_id_attr(attr_item, category_type):
    if category_type == 'clothes':
        attr_id_dict = {
            'TYPE': 8229,
            'BRAND': 31,
            'GENDER': 9163,
            'COLOR': 10096,
            'COLLECTION': 4503,
            'SEASON': 4495,
            'TARGET_AUDIENCE': 9390
        }
        return attr_id_dict[attr_item]
    elif category_type == 'interior':
        attr_id_dict = {
            'TYPE': 8229,
            'BRAND': 85
        }
        return attr_id_dict[attr_item]
    elif category_type == 'decoration':
        attr_id_dict = {
            'TYPE': 8229,
            'BRAND': 85,
            'GENDER': 9163,
            'TARGET_AUDIENCE': 9390,
            'AVERAGE_WEIGHT': 4383,
        }
        return attr_id_dict[attr_item]
    elif category_type == 'accessories':
        attr_id_dict = {
            'TYPE': 8229,
            'BRAND': 85,
            'GENDER': 9163
        }
        return attr_id_dict[attr_item]


def fill_attr_dict(attr_id, attr_value):
    attr_dict = {
        'id': attr_id,
        'values': [{
            'value': attr_value
        }]
    }
    return attr_dict


def get_attributes_list(ecwid_attributes, attr_names_list, category_type, ozon_category_id):
    ozon_attr_list = []
    ecwid_has_this_attr_list = []
    for attr_item in attr_names_list:
        for ecwid_attr_item in ecwid_attributes:
            # случай для бренда, цвета, размера, пола - они есть в эквиде
            if attr_item == ecwid_attr_item['type']:
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value=str(ecwid_attr_item['value']))
                ozon_attr_list.append(attr_dict)
                ecwid_has_this_attr_list.append(attr_item)
    # цикл по аттрибутам, которых нет в эквиде
    for attr_item in attr_names_list:
        if attr_item not in ecwid_has_this_attr_list:
            if attr_item == 'TYPE':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value=get_name_category_dict(ozon_category_id))
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'BRAND':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Бренд')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'GENDER' and category_type != 'interior':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Для детей')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'COLOR' and category_type == 'clothes':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Чёрный')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'COLLECTION' and category_type == 'clothes':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Переходящая модель')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'SEASON' and category_type == 'clothes':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Демисезон')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'TARGET_AUDIENCE' and (category_type == 'clothes' or category_type == 'decoration'):
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value='Для взрослых')
                ozon_attr_list.append(attr_dict)
            elif attr_item == 'AVERAGE_WEIGHT' and category_type == 'decoration':
                attr_dict = fill_attr_dict(attr_id=get_id_attr(attr_item, category_type),
                                           attr_value=0)
                ozon_attr_list.append(attr_dict)
    return ozon_attr_list


def get_attr_from_category(ozon_category_id, ecwid_attributes):
    # Обязательные аттрибуты категорий
    # Одежда: тип, бренд, пол, цвет, коллекция, сезон, целевая аудитория
    clothes_attr_list = ['TYPE', 'BRAND', 'GENDER', 'TARGET_AUDIENCE', 'COLOR', 'COLLECTION', 'SEASON']
    # Украшения: бренд, тип, пол, целевая аудитория, проба, средний вес (г)
    decoration_attr_list = ['TYPE', 'BRAND', 'GENDER', 'TARGET_AUDIENCE', 'AVERAGE_WEIGHT']
    # Аксессуары: тип, бренд, пол
    accessories_attr_list = ['TYPE', 'BRAND', 'GENDER']
    # Интерьер: тип, бренд
    interior_attr_list = ['TYPE', 'BRAND']
    if ozon_category_id in get_category_clothes_id_list():
        return get_attributes_list(ecwid_attributes, clothes_attr_list, 'clothes', ozon_category_id)
    elif ozon_category_id in get_category_interior_id_list():
        return get_attributes_list(ecwid_attributes, interior_attr_list, 'interior', ozon_category_id)
    elif ozon_category_id in get_category_decoration_id_list():
        return get_attributes_list(ecwid_attributes, decoration_attr_list, 'decoration', ozon_category_id)
    elif ozon_category_id in get_category_accessories_id_list():
        return get_attributes_list(ecwid_attributes, accessories_attr_list, 'accessories', ozon_category_id)
