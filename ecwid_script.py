import requests
import os
from dotenv import load_dotenv
import pandas as pd
from general_script import write_to_excel

from ozon_script import Ozon

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

HOST = os.environ.get('ECWID_HOST')
STORE_ID = os.environ.get('ECWID_STORE_ID')
PUBLIC_TOKEN = os.environ.get('ECWID_PUBLIC_TOKEN')
SECRET_TOKEN = os.environ.get('ECWID_SECRET_TOKEN')
CLIENT_ID = os.environ.get('ECWID_CLIENT_ID')
CLIENT_SECRET = os.environ.get('ECWID_CLIENT_SECRET')


class EcwidInterface:
    id_list = []  # id товара
    quantity_list = []  # количество товара на складе ecwid
    name_list = []  # наименование товара
    price_list = []  # цена товара
    options_list = []  # список опций, например, доступные размеры для обуви - 39, 40, 41 и т.д.
    description_list = []  # описание товара
    images_list = []  # список изображений (python list)
    categories_list = []  # список категорий (python list)
    attributes_list = []  # список атрибутов: цвет, размер
    combinations_list = []  # комбинации с другими товарами из ecwid. Сделал на всякий случай
    # dimensions_list = []  # длина, высота, ширина


class EcwidAbstract(EcwidInterface):

    @staticmethod
    def get_loops_count(url):
        url_limit = f'{url}&limit=1'
        result_limit = requests.get(url_limit)
        total_items = result_limit.json()['total']
        len_total_items = len(str(total_items))
        loops_count = 0
        if len_total_items < 3:
            loops_count = 1
        elif len_total_items == 3:
            loops_count = int(str(total_items)[0]) + 1
        elif len_total_items == 4:
            loops_count = int(str(total_items)[0:2]) + 1
        return loops_count

    @staticmethod
    def get_items_offset(i, url):
        if i == 0:
            url_offset = f'{url}&offset={i}'
        else:
            url_offset = f'{url}&offset={i}00'
        # print(url_offset)
        result_offset = requests.get(url_offset)
        items_offset = result_offset.json()['items']
        return items_offset


class Ecwid(EcwidAbstract):

    def get_ecwid_products(self):
        url = f'{HOST}{STORE_ID}/products?token={SECRET_TOKEN}&enabled=false&inStock=true'
        for i in range(0, self.get_loops_count(url)):
            if i > 0:
                break
            items_offset = self.get_items_offset(i, url)
            for count, item in enumerate(items_offset, 1):
                if count > 10:
                    break
                current_item_image_list = []
                self.id_list.append(item['id'])
                self.quantity_list.append(item['quantity'])
                self.name_list.append(item['name'])
                self.price_list.append(item['price'])
                self.options_list.append(item['options'])
                self.description_list.append(item['description'])
                for item_image in item['media']['images']:
                    current_item_image_list.append(item_image['imageOriginalUrl'])
                self.images_list.append(current_item_image_list)
                self.categories_list.append(item['categoryIds'])
                self.attributes_list.append(item['attributes'])
                self.combinations_list.append(item['combinations'])
                # dimensions_list.append(item['dimensions'])
        df = pd.DataFrame({
            'id': self.id_list,
            'quantity': self.quantity_list,
            'name': self.name_list,
            'price': self.price_list,
            'options': self.options_list,
            'description': self.description_list,
            'images': self.images_list,
            'categories': self.categories_list,
            'attributes': self.attributes_list,
            'combinations': self.combinations_list
            # 'dimensions': self.dimensions_list
        })
        write_to_excel(df=df, file_name='ecwid_products')
        # print(df)
        Ozon().import_products_to_ozon(df)
        print('done')

    def get_ecwid_categories(self):
        url = f'{HOST}{STORE_ID}/categories?token={SECRET_TOKEN}&productIds=true'
        count = 0
        for i in range(0, self.get_loops_count(url)):
            items_offset = self.get_items_offset(i, url)
            for item in items_offset:
                # print(count, item['id'], item['name'])
                self.name_list.append(item['name'])
                self.categories_list.append(item['id'])
            count += 1
        df = pd.DataFrame({
            'id': self.categories_list,
            'name': self.name_list
        })
        write_to_excel(df=df, file_name='ecwid_categories')
        print(df)
        print('done')
