import os
import pandas as pd


def write_to_excel(df, file_name):
    path = os.path.join(os.path.dirname(__file__), 'files/')
    if not os.path.exists(path):
        os.mkdir(path)
    writer = pd.ExcelWriter(f'files/{file_name}.xlsx', engine='xlsxwriter')
    df.to_excel(writer, 'Sheet1')
    writer.save()


def print_ecwid_ozon_categories():
    file_name = 'files/ecwid_ozon_categories.xlsx'
    df_excel = pd.read_excel(file_name)
    for index, row in df_excel.iterrows():
        category_id_ecwid = row['category_id_ecwid']
        category_name_ecwid = row['category_name_ecwid']
        category_id_ozon = row['category_id_ozon']
        category_name_ozon = row['category_name_ozon']
        print(f"'{category_id_ecwid}': {category_id_ozon},  # {category_name_ecwid} - {category_name_ozon}")
