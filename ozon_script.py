import requests
import os
from dotenv import load_dotenv
import pandas as pd
from general_script import write_to_excel
from dictionaries import get_category_mapping_dict, get_test_request_data_for_import_ozon, get_attr_from_category

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

HOST = os.environ.get('OZON_HOST')
CLIENT_ID = os.environ.get('OZON_CLIENT_ID')
API_KEY = os.environ.get('OZON_API_KEY')
CONTENT_TYPE = os.environ.get('OZON_CONTENT_TYPE')
HEADERS = {'Api-Key': API_KEY, 'Client-Id': CLIENT_ID}

TEST_HOST = os.environ.get('OZON_TEST_HOST')
TEST_CLIENT_ID = os.environ.get('OZON_TEST_CLIENT_ID')
TEST_API_KEY = os.environ.get('OZON_TEST_API_KEY')
TEST_HEADERS = {'Api-Key': TEST_API_KEY, 'Client-Id': TEST_CLIENT_ID}


class Ozon:
    url_categories = f'{HOST}v1/categories/tree'
    category_names_list = []
    category_id_list = []
    types_list = []
    id_attr_list = []
    name_attr_list = []
    descr_attr_list = []
    type_attr_list = []

    def get_ozon_categories(self, category_id, excel_file_name):
        url = f'{self.url_categories}/{category_id}'
        response = requests.get(url, headers=HEADERS)
        json_response = response.json()['result']
        for item_parent_1 in json_response:
            self.category_names_list.append(item_parent_1['title'])
            self.category_id_list.append(item_parent_1['category_id'])
            self.types_list.append('parent_1')
            if len(item_parent_1['children']) > 0:
                for item_parent_2 in item_parent_1['children']:
                    self.category_names_list.append(item_parent_2['title'])
                    self.category_id_list.append(item_parent_2['category_id'])
                    self.types_list.append('parent_2')
                    if len(item_parent_2['children']) > 0:
                        for item_child in item_parent_2['children']:
                            self.category_names_list.append(item_child['title'])
                            self.category_id_list.append(item_child['category_id'])
                            self.types_list.append('child')
        df = pd.DataFrame({
            'id': self.category_id_list,
            'name': self.category_names_list,
            'type': self.types_list
        })
        write_to_excel(df=df, file_name=excel_file_name)
        print(df)
        print('done')

    def get_attributes_from_category(self, excel_file_name, category_id):
        url = f'{HOST}v2/category/attribute'
        # category_id = 15621031
        data_json = {'category_id': category_id}
        # url = f'{TEST_HOST}v1/categories/tree/{category_id}'
        # data = {'category_id': category_id, 'attribute_type': 'required', 'language': 'RU'}
        response = requests.post(url, json=data_json, headers=HEADERS)
        json_response = response.json()['result']
        for item in json_response:
            # print(item['id'], item['name'])
            self.id_attr_list.append(item['id'])
            self.name_attr_list.append(item['name'])
            self.descr_attr_list.append(item['description'])
            self.type_attr_list.append(item['type'])
        df = pd.DataFrame({
            'id': self.id_attr_list,
            'name': self.name_attr_list,
            'descr': self.descr_attr_list,
            'type': self.type_attr_list
        })
        write_to_excel(df=df, file_name=excel_file_name)
        print(df)
        print('done')
        # print(json_response)
        print(1111)

    @staticmethod
    def import_products_to_ozon(df):
        category_mapping_dict = get_category_mapping_dict()
        test_request_data = get_test_request_data_for_import_ozon()
        product_list = []
        for index, row in df.iterrows():
            # attr_list = []
            ozon_category_id = category_mapping_dict[str(row['categories'][0])]
            # print(ozon_category_id)
            attr_list = get_attr_from_category(ozon_category_id, row['attributes'])
            item_body = {
                'name': str(row['name']),  # наименование товара
                'price': str(row['price']),  # цена
                'barcode': str(row['id']),  # штрихкод. Он же ecwid_product_id
                'images': row['images'],  # список url картинок
                'category_id': ozon_category_id,  # id категории из OZON
                'depth': 0,  # глубина упаковки
                'dimension_unit': 'mm',  # единица измерения габаритов, mm, cm, in
                'height': 0,  # высота упаковки
                'weight': 0,  # вес товара в упаковке
                'weight_unit': 'g',  # единица измерения веса, g, kg, lb (фунты)
                'width': 0,  # ширина упаковки
                'vat': '0',  # ставка НДС, 0 - нет НДС, 0.1 - 10%, 0.2 - 20%
                'attributes': attr_list
            }
            product_list.append(item_body)
        request_data = {'items': product_list}
        # print(request_data)
        url_import = f'{TEST_HOST}v2/product/import'
        print(request_data)
        # print(test_request_data)
        # response = requests.post(url_import, json=request_data, headers=TEST_HEADERS)
        # json_response = response.json()
        # print(json_response)
